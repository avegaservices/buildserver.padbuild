﻿using Khepri.PlayAssetDelivery.Editor;
using UnityEditor;

public class PerformPadBuildTask : PerformAndroidBuildTask
{
	protected override string GetBuildPath(string buildDir, string buildType)
	{
		string fn = $"{PlayerSettings.applicationIdentifier}_{PlayerSettings.bundleVersion}_{GetVersionCode()}_{buildType}";
		return $"{buildDir}/{fn}.aab";
	}

	protected override bool IsAABBuild() => true;

#if UNITY_2020_1_OR_NEWER
	protected override void ConfigureBuildType(string buildType)
	{
		base.ConfigureBuildType(buildType);
		PlayerSettings.Android.useAPKExpansionFiles = false;
	}
#endif

	protected override bool MakeBuild(string outputDir, string outputPath, params string[] scenes) =>
		AssetPackBuilder.BuildBundleWithAssetPacks(new BuildPlayerOptions
		{
			scenes = scenes,
			targetGroup = TargetGroup,
			target = Target,
			options = BuildOptions.None,
			locationPathName = outputPath
		}, EditorUserBuildSettings.androidBuildSubtarget, null);
}
